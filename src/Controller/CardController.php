<?php

namespace App\Controller;

use App\Entity\Order;
use App\Entity\OrderDetail;
use App\Entity\Product;
use App\Entity\ProductVariant;
use App\Service\CardService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\RadioType;
use Symfony\Component\Form\Extension\Core\Type\TelType;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
/**
 * @Route("/sepet", name="card_")
 */
class CardController extends BaseController
{

    /**
     * @Route("/goster", name="view")
     */
    public function view(Request $request)
    {
        $breadcrumb = [
            'title' => 'Sepetim',
            'home' => 'Anasayfa'
        ];

        return $this->render($this->temePath.'/card/view.html.twig',['breadcrumb'=>$breadcrumb,'cardservice'=>$this->cardService]);
    }

    /**
     * @Route("/ode", name="checkout")
     */
    public function checkout(Request $request)
    {
        $defaultData = $this->cardService->getCustomer()->toArray();
        $form = $this->createFormBuilder($defaultData)
            ->add('firstName', TextType::class)
            ->add('lastName', TextType::class)
            ->add('email', EmailType::class)
            ->add('phone', TelType::class)
            ->add('submit', SubmitType::class)
            ->add('paymentMethod', ChoiceType::class,
                array(
                    'choices' => array(
                    'Havale/EFT' => '1'),
                    'multiple'=>false,
                    'expanded'=>true,
                    'data'=>'1')
            )
            ->getForm();

        $form->handleRequest($request);

        if ($request->isMethod('POST')) {

            if ($form->isSubmitted() && $form->isValid()) {
                // perform some action...
                $order = $this->createOrder($form->get('paymentMethod')->getData());
                return $this->redirectToRoute('order_complete',['orderId'=>$order->getOrderId()]);

            }
        }
        $breadcrumb = [
            'title' => 'Sepetim - Ödeme',
            'home' => 'Anasayfa'
        ];

        return $this->render($this->temePath.'/card/checkout.html.twig',['form'=>$form->createView(),'breadcrumb'=>$breadcrumb,'cardservice'=>$this->cardService]);
    }

    /**
     * @Route("/ekle", name="add")
     */
    public function add(Request $request)
    {
        $referer = $request->headers->get('referer');

        $productVariant = $this->getDoctrine()->getRepository(ProductVariant::class)->find($request->request->get('variant'));
        $this->cardService->addProduct($productVariant,$request->request->get('quantity'));
        return new RedirectResponse($referer);
    }

    /**
     * @Route("/ekle/{variantId}", name="add_variant")
     */
    public function addVariant(Request $request, $variantId)
    {
        $referer = $request->headers->get('referer');

        $productVariant = $this->getDoctrine()->getRepository(ProductVariant::class)->find($variantId);
        $this->cardService->addProduct($productVariant,1);
        return new RedirectResponse($referer);
    }

    /**
     * @Route("/sil/{variantId}", name="remove")
     */
    public function remove(Request $request, $variantId)
    {
        $referer = $request->headers->get('referer');
        $productVariant = $this->getDoctrine()->getRepository(ProductVariant::class)->find($variantId);
        $this->cardService->removeProduct($productVariant);
        return new RedirectResponse($referer);
    }

    /**
     * @Route("/favori/{productId}", name="add_remove_favorite")
     */
    public function favorite(Request $request, $productId)
    {
        $referer = $request->headers->get('referer');

        $this->cardService->addFavorite($productId);
        return new RedirectResponse($referer);
    }

    private function createOrder($paymentMethod)
    {
        $customer = $this->cardService->getCustomer();

        $basket = $this->cardService->getBasket();

        $order = new Order();
        $order->setFirstName($customer['firstName']);
        $order->setLastName($customer['lastName']);
        $order->setEmail($customer['email']);
        $order->setPhone($customer['phone']);
        $order->setPaymentMethod(intval($paymentMethod));
        $order->setTotal($basket['total']);
        $order->setTax($basket['total'] * 0.18);
        $order->setStatus('o');
        $order->setDateOrder(new \DateTime('now'));
        $order->setOrderId(date('ymdH').uniqid());
        $this->getDoctrine()->getManager()->persist($order);
        $this->getDoctrine()->getManager()->flush();

        /**
         * @var ProductVariant $item
         */
        foreach ($basket['items'] as $item)
        {
            $orderDetail = new OrderDetail();
            $orderDetail->setOrder($order);
            $orderDetail->setProduct($item['product']);
            $orderDetail->setPrice($item['product']->getPrice());
            $orderDetail->setQuantity($item['quantity']);
            $this->getDoctrine()->getManager()->persist($orderDetail);
            $this->getDoctrine()->getManager()->flush();
        }
        return $order;
    }
}
