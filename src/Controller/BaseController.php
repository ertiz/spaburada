<?php


namespace App\Controller;


use App\Entity\Order;
use App\Service\CardService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class BaseController  extends AbstractController
{
    public $temePath = 'multikart';

    public $cardService;

    /**
     * @var \Swift_Mailer
     */
    public $mailler;
    /**
     * BaseController constructor.
     */
    public function __construct(CardService $cardService, \Swift_Mailer $mailer)
    {
        $this->cardService = $cardService;
        $this->mailler = $mailer;

    }


    public function sendOrderComplete(Order $order)
    {
        $message = (new \Swift_Message('SpaBurada - Sipariş Tamamlandı'))
            ->setFrom('siparis@spaburada.com')
            ->setTo($order->getEmail())
            ->setBody(
                $this->renderView(
                // templates/emails/registration.html.twig
                    $this->temePath.'/emails/order_complete.html.twig',
                    ['order' => $order]
                ),
                'text/html'
            )
        ;


        $sonuc = $this->mailler->send($message);
    }
}