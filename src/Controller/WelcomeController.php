<?php

namespace App\Controller;

use App\Entity\Product;
use Symfony\Component\Routing\Annotation\Route;

class WelcomeController extends BaseController
{

    /**
     * @Route("/", name="index")
     */
    public function index()
    {


        $products = $this->getDoctrine()->getRepository(Product::class)->getProductVariant();


        return $this->render($this->temePath.'/welcome.html.twig',['products'=>$products,'cardservice'=>$this->cardService]);
    }

    /**
     * @Route("/{productlink}", name="index")
     */
    public function product($productlink)
    {
        setlocale(LC_TIME, 'tr_TR');

        /**
         * @var Product $product
         */
        $product = $this->getDoctrine()->getRepository(Product::class)->findOneBy(['link'=>$productlink]);
        if(!$product)
        {
            return $this->index();
        }


        $breadcrumb = [
            'title' => $product->getTitle(),
            'home' => 'Anasayfa'
        ];
        $expireDate = strftime("%d %B %Y", $product->getExpireDate()->getTimestamp());
        return $this->render($this->temePath.'/product-detail.html.twig',['product'=>$product,'expireDate'=>$expireDate,'cardservice'=>$this->cardService, 'breadcrumb' => $breadcrumb]);
    }

    /**
     * @Route("/hakkimizda", name="aboutus")
     */
    public function aboutus()
    {

        $breadcrumb = [
            'title' => 'Spaburada  Hakkında',
            'home' => 'Anasayfa'
        ];

        return $this->render($this->temePath.'/about-us.html.twig',['breadcrumb' => $breadcrumb,'cardservice'=>$this->cardService]);
    }
}
