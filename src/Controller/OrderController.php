<?php

namespace App\Controller;

use App\Entity\Order;
use App\Entity\OrderDetail;
use App\Entity\Product;
use App\Entity\ProductVariant;
use App\Service\CardService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\RadioType;
use Symfony\Component\Form\Extension\Core\Type\TelType;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
/**
 * @Route("/siparis", name="order_")
 */
class OrderController extends BaseController
{

    /**
     * @Route("/tamamlandi/{orderId}", name="complete")
     */
    public function complete(Request $request, $orderId)
    {

        setlocale(LC_TIME, 'tr_TR');

        $breadcrumb = [
            'title' => 'Sipariş - Tamamlandı',
            'home' => 'Anasayfa'
        ];

        /**
         * @var Order $order
         */
        $order = $this->getDoctrine()->getRepository(Order::class)->findOneBy(['orderId'=>$orderId]);

        $orderDate = strftime("%d %B %Y", $order->getDateOrder()->getTimestamp());

        $paymentMethod = 'Banka Havalesi / EFT';
        switch ($order->getPaymentMethod())
        {
            case 1:
                $paymentMethod = 'Banka Havalesi / EFT';
                break;
        }

        $this->sendOrderComplete($order);

        return $this->render($this->temePath.'/order/complete.html.twig',[
            'breadcrumb'=>$breadcrumb,
            'cardservice'=>$this->cardService,
            'order'=>$order,
            'orderDate'=>$orderDate,
            'paymentMethod'=>$paymentMethod
        ]);
    }

}
