<?php

namespace App\Controller;

use App\Entity\Product;
use App\Service\CardService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class PageController extends BaseController
{


    /**
     * @Route("/kullanim-kosullari", name="termofuse")
     */
    public function termofuse()
    {

        $breadcrumb = [
            'title' => 'Kullanım Koşulları',
            'home' => 'Anasayfa'
        ];

        return $this->render($this->temePath.'/termofuse.html.twig',['breadcrumb'=>$breadcrumb,'cardservice'=>$this->cardService]);
    }

    /**
     * @Route("/gizlilik-politikasi", name="privacy")
     */
    public function privacy()
    {

        $breadcrumb = [
            'title' => 'Gizlilik Politikası',
            'home' => 'Anasayfa'
        ];

        return $this->render($this->temePath.'/privacy.html.twig',['breadcrumb'=>$breadcrumb,'cardservice'=>$this->cardService]);
    }
    /**
     * @Route("/kisisel-verilerin-korunmasi", name="userpermission")
     */
    public function userpermission()
    {

        $breadcrumb = [
            'title' => 'Kişisel Verilerin Korunması',
            'home' => 'Anasayfa'
        ];

        return $this->render($this->temePath.'/userpermission.html.twig',['breadcrumb'=>$breadcrumb,'cardservice'=>$this->cardService]);
    }

    /**
     * @Route("/sikca-sorulan-sorular", name="faq")
     */
    public function faq()
    {

        $breadcrumb = [
            'title' => 'Sıkça Sorulan Sorular',
            'home' => 'Anasayfa'
        ];

        return $this->render($this->temePath.'/faq.html.twig',['breadcrumb'=>$breadcrumb,'cardservice'=>$this->cardService]);
    }

    /**
     * @Route("/insan-kaynaklari", name="humanresource")
     */
    public function humanresource()
    {

        $breadcrumb = [
            'title' => 'İnsna Kaynakları',
            'home' => 'Anasayfa'
        ];

        return $this->render($this->temePath.'/humanresource.html.twig',['breadcrumb'=>$breadcrumb,'cardservice'=>$this->cardService]);
    }

    /**
     * @Route("/iletisim", name="contact")
     */
    public function contact()
    {

        $breadcrumb = [
            'title' => 'İnsna Kaynakları',
            'home' => 'Anasayfa'
        ];

        return $this->render($this->temePath.'/contact.html.twig',['breadcrumb'=>$breadcrumb,'cardservice'=>$this->cardService]);
    }
}
