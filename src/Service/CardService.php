<?php


namespace App\Service;


use App\Entity\ProductVariant;
use App\Repository\ProductVariantRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\HttpFoundation\Session\SessionInterface;

class CardService
{
    /**
     * @var SessionInterface
     */
    private $session;

    /**
     * @var ArrayCollection
     */
    private $hold;

    /**
     * @var ArrayCollection
     */
    private $favorite;
    /**
     * @var ProductVariantRepository
     */
    private $repo;

    /**
     * @var array
     */
    private $customer;
    /**
     * CardService constructor.
     * @param SessionInterface $session
     */
    public function __construct(SessionInterface $session, ProductVariantRepository $productVariantRepository)
    {
        $this->session = $session;
        $this->hold = new ArrayCollection($this->session->get('basket') ?? []);
        $this->customer = new ArrayCollection($this->session->get('customer') ?? ['firstName'=>'','lasttName'=>'','phone'=>'','email'=>'', 'paymentMethod'=>1]);
        $this->favorite = new ArrayCollection($this->session->get('favorite') ?? []);
        $this->repo = $productVariantRepository;
    }

    public function addProduct(ProductVariant $productVariant, $quantity)
    {
        if($this->hold->containsKey($productVariant->getId()))
        {
            $quantity += $this->hold->get($productVariant->getId())['quantity'];
        }
        $this->hold->set($productVariant->getId(),[
            'quantity' => $quantity,
            'product' => $productVariant->getId(),
        ]);
        $this->updateSession();
    }
    public function removeProduct(ProductVariant $productVariant)
    {
        if($this->hold->containsKey($productVariant->getId()))
        {
            $this->hold->remove($productVariant->getId());
        }
        $this->updateSession();
    }

    public function addFavorite($productId)
    {
        if(!$this->hold->contains($productId))
        {
            $this->favorite->add($productId);
        }else{
            $this->removeFavorite($productId);
        }

        $this->updateSession();
    }
    public function removeFavorite($productId)
    {
        if($this->hold->contains($productId))
        {
            $this->hold->remove($productId);
        }
        $this->updateSession();
    }
    public function getBasket()
    {
        $products = $this->hold->toArray();
        $total = 0;
        array_walk($products,function (&$item) use (&$total){
            $item['product'] = $this->repo->find($item['product']);
            $total += $item['quantity'] * $item['product']->getPrice();
        });

        return [
            'items'=>$products,
            'total'=> $total
            ];
    }
    public function getBasketItems()
    {
       return $this->hold;
    }

    public function isFavorite($productId)
    {
        echo($this->favorite->contains($productId) ? '-broken' : '');
        return $this->favorite->contains($productId) ? '-broken' : '';
    }
    private function updateSession()
    {
        $this->session->clear();
        $this->session->set('basket',$this->hold->toArray());
        $this->session->set('favorite',$this->favorite->toArray());
    }

    /**
     * @return array
     */
    public function getCustomer()
    {
        return $this->customer;
    }

    /**
     * @param array $customer
     */
    public function setCustomer($customer)
    {
        $this->customer = $customer;
        $this->session->set('customer',$customer);
    }
}