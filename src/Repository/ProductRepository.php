<?php

namespace App\Repository;

use App\Entity\Product;
use App\Entity\ProductImage;
use App\Entity\ProductVariant;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;
use Doctrine\ORM\Query\Expr\Join;

/**
 * @method Product|null find($id, $lockMode = null, $lockVersion = null)
 * @method Product|null findOneBy(array $criteria, array $orderBy = null)
 * @method Product[]    findAll()
 * @method Product[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ProductRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Product::class);
    }

    public function getProductVariant()
    {
        $qb = $this->createQueryBuilder('p');

        $qb->join('p.variant', 'pv', 'WITH', 'pv.main = :main');
        $qb->join('p.image', 'pi', 'WITH', 'pi.main = :main');

        $qb->setParameter('main',1);
        return $qb->getQuery()->getResult();
    }
}
