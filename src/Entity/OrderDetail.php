<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Table(name="`order_detail`")
 * @ORM\Entity(repositoryClass="App\Repository\OrderDetailRepository")
 */
class OrderDetail
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @var \Order
     *
     * @ORM\ManyToOne(targetEntity="Order", inversedBy="detail")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="`order`", referencedColumnName="id")
     * })
     */
    private $order;

    /**
     * @var \ProductVariant
     *
     * @ORM\ManyToOne(targetEntity="ProductVariant")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="product", referencedColumnName="id")
     * })
     */
    private $product;

    /**
     * @ORM\Column(type="integer")
     */
    private $quantity;

    /**
     * @ORM\Column(type="decimal", precision=10, scale=3)
     */
    private $price;

    public function getId()
    {
        return $this->id;
    }

    /**
     * @return \Order
     */
    public function getOrder()
    {
        return $this->order;
    }

    /**
     * @param \Order $order
     */
    public function setOrder($order)
    {
        $this->order = $order;
    }

    public function getProduct()
    {
        return $this->product;
    }

    public function setProduct( $product): self
    {
        $this->product = $product;

        return $this;
    }

    public function getQuantity(): ?int
    {
        return $this->quantity;
    }

    public function setQuantity(int $quantity): self
    {
        $this->quantity = $quantity;

        return $this;
    }

    public function getPrice(): ?float
    {
        return $this->price;
    }

    public function setPrice(float $price): self
    {
        $this->price = $price;

        return $this;
    }
}
