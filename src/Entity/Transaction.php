<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Transaction
 *
 * @ORM\Table(name="transaction")
 * @ORM\Entity
 */
class Transaction
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="order", type="string", length=255, nullable=false)
     */
    private $order;

    /**
     * @var string
     *
     * @ORM\Column(name="product", type="string", length=255, nullable=false)
     */
    private $product;

    /**
     * @var string
     *
     * @ORM\Column(name="email", type="string", length=255, nullable=false)
     */
    private $email;

    /**
     * @var string
     *
     * @ORM\Column(name="card_holder_name", type="string", length=255, nullable=false)
     */
    private $cardHolderName;

    /**
     * @var string
     *
     * @ORM\Column(name="pos_ref_id", type="string", length=255, nullable=false)
     */
    private $posRefId;

    /**
     * @var int
     *
     * @ORM\Column(name="quantity", type="integer", nullable=false)
     */
    private $quantity;

    /**
     * @var float
     *
     * @ORM\Column(name="price", type="float", precision=10, scale=0, nullable=false)
     */
    private $price;

    /**
     * @var string
     *
     * @ORM\Column(name="status", type="string", length=255, nullable=false)
     */
    private $status;

    /**
     * @var string
     *
     * @ORM\Column(name="pos_request", type="text", length=0, nullable=false)
     */
    private $posRequest;

    /**
     * @var string
     *
     * @ORM\Column(name="pos_response", type="text", length=0, nullable=false)
     */
    private $posResponse;

    /**
     * @var string
     *
     * @ORM\Column(name="transaction_time", type="string", length=255, nullable=false)
     */
    private $transactionTime;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getOrder(): ?string
    {
        return $this->order;
    }

    public function setOrder(string $order): self
    {
        $this->order = $order;

        return $this;
    }

    public function getProduct(): ?string
    {
        return $this->product;
    }

    public function setProduct(string $product): self
    {
        $this->product = $product;

        return $this;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    public function getCardHolderName(): ?string
    {
        return $this->cardHolderName;
    }

    public function setCardHolderName(string $cardHolderName): self
    {
        $this->cardHolderName = $cardHolderName;

        return $this;
    }

    public function getPosRefId(): ?string
    {
        return $this->posRefId;
    }

    public function setPosRefId(string $posRefId): self
    {
        $this->posRefId = $posRefId;

        return $this;
    }

    public function getQuantity(): ?int
    {
        return $this->quantity;
    }

    public function setQuantity(int $quantity): self
    {
        $this->quantity = $quantity;

        return $this;
    }

    public function getPrice(): ?float
    {
        return $this->price;
    }

    public function setPrice(float $price): self
    {
        $this->price = $price;

        return $this;
    }

    public function getStatus(): ?string
    {
        return $this->status;
    }

    public function setStatus(string $status): self
    {
        $this->status = $status;

        return $this;
    }

    public function getPosRequest(): ?string
    {
        return $this->posRequest;
    }

    public function setPosRequest(string $posRequest): self
    {
        $this->posRequest = $posRequest;

        return $this;
    }

    public function getPosResponse(): ?string
    {
        return $this->posResponse;
    }

    public function setPosResponse(string $posResponse): self
    {
        $this->posResponse = $posResponse;

        return $this;
    }

    public function getTransactionTime(): ?string
    {
        return $this->transactionTime;
    }

    public function setTransactionTime(string $transactionTime): self
    {
        $this->transactionTime = $transactionTime;

        return $this;
    }


}
